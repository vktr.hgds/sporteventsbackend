require 'test_helper'

class Api::V1::EventsControllerTest < ActionController::TestCase
  test 'sport is not found' do
    get :index, params: { sport_id: 'abc' }
    assert_response :not_found
  end

  test 'sport is found, successful response' do
    get :index, params: { sport_id: Sport.first&.id }
    assert_response :success
  end
end