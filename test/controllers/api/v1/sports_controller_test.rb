require 'test_helper'

class Api::V1::SportsControllerTest < ActionController::TestCase
  fixtures :sports

  def setup
    pos_values = Sport.all.pluck(:pos)
    @max_pos_val = pos_values.max
    @min_pos_val = pos_values.min
  end

  test 'no pos parameter is provided, successful response' do
    get :index

    assert_response :success
  end

  test 'no pos parameter is provided, first item has lowest pos' do
    get :index

    assert_equal @response.parsed_body&.first.dig('pos'), @min_pos_val
  end

  test 'pos is provided with desc value, first item has highest pos' do
    get :index, params: { pos: 'desc' }

    assert_equal @response.parsed_body&.first.dig('pos'), @max_pos_val
  end

  test 'pos is provided with desc value, first item has lowest pos' do
    get :index, params: { pos: 'asc' }

    assert_equal @response.parsed_body&.first.dig('pos'), @min_pos_val
  end
end