require 'test_helper'

class Api::V1::OutcomesControllerTest < ActionController::TestCase
  test 'sport is not found' do
    get :index, params: { sport_id: 'abc', event_id: 'abc' }
    assert_response :not_found
  end

  test 'event is not found' do
    get :index, params: { sport_id: Sport.first&.id, event_id: 'abc' }
    assert_response :not_found
  end

  test 'sport and event are found, successful response' do
    get :index, params: { sport_id: Sport.first&.id, event_id: Event.first&.id }
    assert_response :success
  end
end