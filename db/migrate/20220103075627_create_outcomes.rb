class CreateOutcomes < ActiveRecord::Migration[6.1]
  def change
    create_table :outcomes do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.references :event, null: false, foreign_key: true
      t.references :home, index: true, foreign_key: { to_table: :competitors }
      t.references :away, index: true, foreign_key: { to_table: :competitors }
      t.string :home_score
      t.string :away_score

      t.timestamps
    end
  end
end
