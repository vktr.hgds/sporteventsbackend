class AddLeagues < ActiveRecord::Migration[6.1]
  def change
    League.create!(name: 'UEFA Champions League', sport_id: Sport.find_by_name('Football')&.id)
    League.create!(name: 'EHF Champions League', sport_id: Sport.find_by_name('Handball')&.id)
    League.create!(name: 'NBA', sport_id: Sport.find_by_name('Basketball')&.id)
    League.create!(name: 'Darts WC', sport_id: Sport.find_by_name('Darts')&.id)
    League.create!(name: 'Roland Garros', sport_id: Sport.find_by_name('Tennis')&.id)
  end
end
