class AddEvents < ActiveRecord::Migration[6.1]
  def change
    rounds = Round.where(name: 'Round of 16').pluck(:id)
    sports = Sport.all
    i = 0

    rounds.each do |round|
      Event.create!(
        round_id: round,
        sport_id: sports[i].id,
        start_date: DateTime.now - rand(1..10).days,
        end_date: DateTime.now + rand(1..10).days
      )

      i += 1
    end
  end
end
