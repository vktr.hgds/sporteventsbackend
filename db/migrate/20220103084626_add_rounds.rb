class AddRounds < ActiveRecord::Migration[6.1]
  def change
    round_names = ['Round of 16', 'Quarter final', 'Semi final', 'Final']
    leagues = League.all

    leagues.each do |league|
      round_names.each do |round_name|
        Round.create!(name: round_name, league_id: league&.id)
      end
    end
  end
end
