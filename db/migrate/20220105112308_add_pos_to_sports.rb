class AddPosToSports < ActiveRecord::Migration[6.1]
  def change
    add_column :sports, :pos, :integer
  end
end
