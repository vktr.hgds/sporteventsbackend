class AddSports < ActiveRecord::Migration[6.1]
  def change
    sports_name = %w[Football Tennis Darts Basketball Handball]

    sports_name.each do |sport_name|
      Sport.create!(name: sport_name)
    end
  end
end
