class AddCompetitors < ActiveRecord::Migration[6.1]
  def change
    football_teams = %w[MU MCI CHE LIV FCB BAR RMA PSG ATM JUV]
    basketball_teams = %w[Lakers Bulls Nets Knicks]
    handball_teams = %w[PICK KIEL PSG VESZPREM]

    darts_players = %w[Wright Anderson Gerwen Barneveld]
    tennis_players = %w[Federer Djokovic Murray Nadal]

    add_to_db(football_teams, Sport.find_by_name('Football')&.id)
    add_to_db(basketball_teams, Sport.find_by_name('Basketball')&.id)
    add_to_db(handball_teams, Sport.find_by_name('Handball')&.id)
    add_to_db(darts_players, Sport.find_by_name('Darts')&.id)
    add_to_db(tennis_players, Sport.find_by_name('Tennis')&.id)
  end

  def add_to_db(competitors, type)
    competitors.each do |competitor_name|
      Competitor.create!(name: competitor_name, sport_id: type)
    end
  end
end
