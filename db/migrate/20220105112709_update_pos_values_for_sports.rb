class UpdatePosValuesForSports < ActiveRecord::Migration[6.1]
  def change
    sports = Sport.all
    number_of_sports = sports.length

    sports.each do |s|
      s.pos = number_of_sports
      s.save!

      number_of_sports -= 1
    end
  end
end
