class AddOutcomes < ActiveRecord::Migration[6.1]
  def change
    round_ids = Event.all.pluck(:id)
    rounds = Round.where(id: round_ids)
    football_round = rounds.detect { |round| round.league.sport.name == 'Football' }
    football_teams = Competitor.where(sport_id: Sport.find_by_name('Football'))
    i = 0

    while i < football_teams.size - 1
      Outcome.create!(
        start_date: DateTime.now - 1.days,
        end_date: DateTime.now + rand(1..2).days,
        event_id: football_round&.events&.first&.id,
        home_id: football_teams[i]&.id,
        away_id: football_teams[i + 1]&.id,
        home_score: rand(1..10).to_s,
        away_score: rand(1..10).to_s,
        )

      i += 2
    end
  end
end
