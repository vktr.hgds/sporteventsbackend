# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_05_112709) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "competitors", force: :cascade do |t|
    t.string "name"
    t.bigint "sport_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sport_id"], name: "index_competitors_on_sport_id"
  end

  create_table "events", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.bigint "round_id", null: false
    t.bigint "sport_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["round_id"], name: "index_events_on_round_id"
    t.index ["sport_id"], name: "index_events_on_sport_id"
  end

  create_table "leagues", force: :cascade do |t|
    t.string "name"
    t.bigint "sport_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sport_id"], name: "index_leagues_on_sport_id"
  end

  create_table "outcomes", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.bigint "event_id", null: false
    t.bigint "home_id"
    t.bigint "away_id"
    t.string "home_score"
    t.string "away_score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["away_id"], name: "index_outcomes_on_away_id"
    t.index ["event_id"], name: "index_outcomes_on_event_id"
    t.index ["home_id"], name: "index_outcomes_on_home_id"
  end

  create_table "rounds", force: :cascade do |t|
    t.string "name"
    t.bigint "league_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["league_id"], name: "index_rounds_on_league_id"
  end

  create_table "sports", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "pos"
  end

  add_foreign_key "competitors", "sports"
  add_foreign_key "events", "rounds"
  add_foreign_key "events", "sports"
  add_foreign_key "leagues", "sports"
  add_foreign_key "outcomes", "competitors", column: "away_id"
  add_foreign_key "outcomes", "competitors", column: "home_id"
  add_foreign_key "outcomes", "events"
  add_foreign_key "rounds", "leagues"
end
