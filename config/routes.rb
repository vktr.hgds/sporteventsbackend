Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
        get '/sports', to: 'sports#index'
        get '/sports/:sport_id/events', to: 'events#index'
        get '/sports/:sport_id/events/:event_id/outcomes', to: 'outcomes#index'
    end
  end
end
