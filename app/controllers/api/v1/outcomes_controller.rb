class Api::V1::OutcomesController < ApplicationController

  before_action :find_sport, :find_outcomes, only: :index

  def index
    render json: @all_outcomes, status: :ok
  end

  private

  def find_outcomes
    event = Event.find_by_sport_id_and_event_id(@sport_as_hash.dig('id'), params[:event_id]).first
    return render status: :not_found unless event

    @all_outcomes = $redis.get("all_outcomes_for_an_event##{event.id}") rescue nil

    unless @all_outcomes.present?
      find_all_outcomes = serialize(event.outcomes, OutcomeSerializer)
      @all_outcomes = find_all_outcomes.to_json
      $redis.set("all_outcomes_for_an_event##{event.id}", @all_outcomes)
      $redis.expire("all_outcomes_for_an_event##{event.id}", 30.minutes.to_i)
    end
  end
end