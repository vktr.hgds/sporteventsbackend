class Api::V1::SportsController < ApplicationController

  before_action :find_all_sports, only: :index

  def index
    render json: @all_sports, status: :ok
  end

  private

  def find_all_sports
    pos = calculate_order_by_pos(params[:pos])
    @all_sports = $redis.get("all_sports##{pos}") rescue nil

    unless @all_sports.present?
      find_all_sports = serialize(Sport.ordered_by_pos(pos), SportSerializer)
      @all_sports = find_all_sports.to_json
      $redis.set("all_sports##{pos}", @all_sports)
      $redis.expire("all_sports##{pos}", 30.minutes.to_i)
    end
  end

  def calculate_order_by_pos(pos)
    return :asc if pos.blank?

    pos_to_sym = pos.downcase&.to_sym
    return :asc unless [:asc, :desc].include?(pos_to_sym)

    pos_to_sym
  end
end