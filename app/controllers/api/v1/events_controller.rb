class Api::V1::EventsController < ApplicationController

  before_action :find_sport, :find_events, only: :index

  def index
    render json: @all_events, status: :ok
  end

  private

  def find_events
    sport_id = @sport_as_hash.dig('id')
    @all_events = $redis.get("all_events_for_a_sport##{sport_id}") rescue nil

    unless @all_events.present?
      find_all_events = serialize(Event.find_all_by_sport_id(sport_id), EventSerializer)
      @all_events = find_all_events.to_json
      $redis.set("all_events_for_a_sport##{sport_id}", @all_events)
      $redis.expire("all_events_for_a_sport##{sport_id}", 30.minutes.to_i)
    end
  end
end