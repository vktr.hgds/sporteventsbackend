class ApplicationController < ActionController::API

  protected

  def find_sport
    sport_id = params[:sport_id]
    sport = $redis.get("sport_by_id##{sport_id}") rescue nil

    unless sport.present?
      find_sport = Sport.find_by_id(sport_id)
      return render status: :not_found unless find_sport

      sport = find_sport.to_json
      $redis.set("sport_by_id##{sport_id}", sport)
      $redis.expire("sport_by_id##{sport_id}", 30.minutes.to_i)
    end

    @sport_as_hash = JSON.parse sport
  end

  def serialize(elements, serializer)
    ActiveModelSerializers::SerializableResource.new(elements, each_serializer: serializer)
  end
end
