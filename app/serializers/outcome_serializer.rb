class OutcomeSerializer < ActiveModel::Serializer
  attributes :id, :competitors, :event, :start_date, :end_date

  attr_accessor :away_competitor, :away_score, :home_competitor, :home_score

  def competitors
    winner = calculate_winner

    {
      home_id: object&.home_id,
      home_competitor: home_competitor,
      home_score: home_score,
      away_id: object&.away_id,
      away_competitor: away_competitor,
      away_score: away_score,
      winner: winner
    }
  end

  def event
    {
      event_id: object&.event_id,
      sport_id: object&.event&.sport&.id,
      sport_name: object&.event&.sport&.name,
      round_id: object&.event&.round&.id,
      round_name: object&.event&.round&.name,
      league_id: object&.event&.round&.league.id,
      league_name: object&.event&.round&.league.name
    }
  end

  private

  def calculate_winner
    @home_score = object&.home_score
    @home_competitor = Competitor.find_by_id(object.home_id)&.name
    @away_score = object&.away_score
    @away_competitor = Competitor.find_by_id(object.away_id)&.name

    return @home_competitor if @home_score > @away_score
    return @away_competitor if @home_score < @away_score

    'Draw'
  end
end
