class SportSerializer < ActiveModel::Serializer
  attributes :id, :name, :pos, :event_count

  def event_count
    object&.events&.count
  end
end
