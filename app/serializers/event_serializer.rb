class EventSerializer < ActiveModel::Serializer
  attributes :id, :event, :start_date, :end_date

  def event
    {
      sport_id: object&.sport&.id,
      sport_name: object&.sport&.name,
      round_id: object&.round&.id,
      round_name: object&.round&.name,
      league_id: object&.round&.league&.id,
      league_name: object&.round&.league&.name,
      outcomes_count: object&.outcomes&.count
    }
  end
end
