class League < ApplicationRecord
  has_many :rounds
  belongs_to :sport
end
