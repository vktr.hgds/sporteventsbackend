class Round < ApplicationRecord
  belongs_to :league
  has_many :events
  has_many :sports, through: :events
end
