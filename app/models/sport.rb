class Sport < ApplicationRecord
  has_one :competitor
  has_many :leagues
  has_many :events
  has_many :rounds, through: :events

  scope :ordered_by_pos, -> (order) { order(pos: order&.to_sym) }
end
