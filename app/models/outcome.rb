class Outcome < ApplicationRecord
  belongs_to :event

  belongs_to :competitor, class_name: "Competitor", foreign_key: 'home_id'
  belongs_to :competitor, class_name: "Competitor", foreign_key: 'away_id'
end
