class Competitor < ApplicationRecord
  belongs_to :sport
  has_many :outcomes
  has_many :events, through: :outcomes
end
