class Event < ApplicationRecord
  belongs_to :round
  belongs_to :sport
  has_many :outcomes
  has_many :competitors, through: :outcomes

  scope :find_all_by_sport_id, -> (sport_id) { where(sport_id: sport_id) }
  scope :find_by_sport_id_and_event_id, -> (sport_id, event_id) { where(sport_id: sport_id, id: event_id) }
end
