# README

Backend API made in Ruby on Rails, serving sport data. Sports, competitions, matches. Sorting operation by "pos" parameter.
Sports are ordered on the backend by the "pos" (position) parameter, however ordering sports on frontend could work as well (React app).

Backend uses Redis for caching, thus the 2nd operation of ordering by pos takes a very short time.

steps:

bundle install
rails db:migrate
rails s

* server starts at localhost:3000

